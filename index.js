// Quizz
// 1. What is the blueprint where objects are created from?
	// - Classes

// 2. What is the naming convention applied to classes?
	// - class names should begin with an uppercase letter

// 3. What keyword do we use to create objects from a class?
	// - new keyword

// 4. What is the technical term for creating an object from a class?
	// - Instantiate

// 5. What class method dictates HOW objects will be created from that class?
	// - class constructor



// Function Coding
// 1. Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.

class Student {
	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		this.grade = grades.every(grade => (grade >= 0 && grade <= 100))? grades : undefined;
	}
}




// 2. Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.

let studentOne = new Student("John", "john@email.com", [101,88,97,75])
let studentTwo = new Student("Jane", "jane@email.com", [87, 99, 83, 76])
let studentThree = new Student("Joe","joe@mail.com", [78, 82, 79, 85])
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93])















